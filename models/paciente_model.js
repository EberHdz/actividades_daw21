const db = require('../util/database');


module.exports= class Paciente{

    constructor(primer_nombre,segundo_nombre,apellido_paterno,apellido_materno,curp,fecha_nacimiento,sexo,calle,municipio,estado,codigo_postal,telefono,correo_electronico,referencia,estado_civil,ocupacion,recibo_fiscal){
        
        
        
        this.primer_nombre=primer_nombre,
        this.segundo_nombre=segundo_nombre,
        this.apellido_paterno=apellido_paterno,
        this.apellido_materno=apellido_materno,
        this.curp=curp,
        this.fecha_nacimiento=fecha_nacimiento,
        this.sexo=sexo,
        this.calle=calle,
        this.municipio=municipio,
        this.estado=estado,
        this.codigo_postal=codigo_postal,
        this.telefono=telefono,
        this.correo_electronico=correo_electronico,
        this.referencia=referencia,
        this.estado_civil=estado_civil,
        this.ocupacion=ocupacion,
        this.recibo_fiscal = recibo_fiscal






    }



    save(){
        return db.execute('INSERT INTO paciente (primer_nombre,segundo_nombre,apellido_paterno,apellido_materno,curp,fecha_nacimiento,sexo,calle,municipio,estado,codigo_postal,telefono,correo_electronico,referencia,estado_civil,ocupacion,recibo_fiscal) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', 
           [this.primer_nombre,
            this.segundo_nombre,
            this.apellido_paterno,
            this.apellido_materno,
            this.curp,
            this.fecha_nacimiento,
            this.sexo,
            this.calle,
            this.municipio,
            this.estado,
            this.codigo_postal,
            this.telefono,
            this.correo_electronico,
            this.referencia,
            this.estado_civil,
            this.ocupacion,
            this.recibo_fiscal
        
        ]);
    }
   
    static fetchAll() {
        return db.execute('SELECT * FROM paciente');       
     }

     
   /*
     static fetchOne(primer_nombre){
         return db.execute('SELECT * FROM paciente WHERE primer_nombre LIKE ',['%'+primer_nombre+'%']);   
     } 

    */
 

  
}

