const express = require('express');
const router = express.Router();
const path= require('path');

//Controladores
const gestor_datos_controller=require('../controllers/gestor_datos_controller')

//Ver Historial
router.get('/pacientes', gestor_datos_controller.getGestorDatos);
router.post('/eliminar', gestor_datos_controller.postGestorPaciente);



//Exportar las rutas 
module.exports = router;
