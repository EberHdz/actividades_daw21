const express = require('express');
const router = express.Router();
const path= require('path');


//Controladores
const paciente_controller=require('../controllers/paciente_controller')

//Nuevo Paciente
router.get('/nuevo_paciente', paciente_controller.getNuevoPaciente);
router.post('/nuevo_paciente', paciente_controller.postNuevoPaciente);
router.get('/nuevo_servicio', paciente_controller.getNuevoServicio);


//Exportar las rutas 
module.exports = router;
