const { request, response } = require('express');
const Paciente= require('../models/paciente_model');


exports.getNuevoPaciente= (request, response, next) => {
    response.render('nuevo_paciente',{title:'Nuevo Paciente'});
} ;



exports.postNuevoPaciente= (request,response,next)=>{
    

  const paciente= new Paciente( 
                                request.body.primer_nombre,
                                request.body.segundo_nombre,
                                request.body.apellido_paterno,
                                request.body.apellido_materno,
                                request.body.curp,
                                request.body.fecha_nacimiento,
                                request.body.sexo,
                                request.body.calle,
                                request.body.municipio,
                                request.body.estado,
                                request.body.codigo_postal,
                                request.body.telefono,
                                request.body.correo_electronico,
                                request.body.referencia,
                                request.body.estado_civil,
                                request.body.ocupacion,
                                request.body.recibo_fiscal);

        paciente.save().then(() => {response.redirect('/empleado'); } ).catch(err => console.log(err));
        
                

    };


    exports.getNuevoServicio = (request, response, next) => {
        Paciente.fetchAll()
        .then(([rows, fieldData]) => {
            response.render('nuevo_servicio',
            {usuarios:rows, title: 'Nuevo Servicio'});
               
            })
            .catch(err => {
            console.log(err);
        });  
      

       
    };
    
    