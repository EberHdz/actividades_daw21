const { request, response } = require('express');
const Paciente= require('../models/gestor_datos_model');

exports.getGestorDatos =  (request, response, next) => {
Paciente.fetchAll()
.then(([rows, fieldData]) => {
    response.render('index_gestor_pacientes',
    {pacientes:rows, title: 'Gestor-Pacientes'});
    
})
.catch(err => {
    console.log(err);
}); 
 
}

exports.postGestorPaciente= (request,response,next)=>{
    
    

    Paciente.delete(request.body.id_paciente)

        .then(()=>{ 
                    
                    Paciente.fetchAll() 
                    .then(([rows, fieldData]) => {
                           return response.status(200).json({pacientes: rows});
                      })
                     .catch(err => { 
                            console.log(err);
                     });
                 
                  }
            )
        .catch((err)=>{
                        console.log(err);
                        return response.status(500).json({message:"Internal Server Error"});
                      }
        );


}

