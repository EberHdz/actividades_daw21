const mysql = require('mysql2');

const pool = mysql.createPool({
    host: 'localhost',
    user: 'root',
    database: 'damas_azules',
    password: ''
    
});

module.exports = pool.promise();