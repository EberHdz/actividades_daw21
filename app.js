const express = require('express');
const app = express(); 

const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
const router=express.Router(); 

//Public
const path= require('path');
app.use(express.static(path.join(__dirname, 'public')));

// /////////////////////////////////////////////////////////
// //Para poder utilizar las cookies
// const cookieParser = require('cookie-parser');
// app.use(cookieParser());
/////////////////////////////////////////////////////////
//AJAX-JSONS
app.use(bodyParser.json());

// /////////////////////////////////////////////////////////
// //Poder utilizar sessiones
// const session = require('express-session');

// app.use(session({
//     secret: 'cjKepemdlOIDHKLSÑIDMkmdsñlmlkdfmlk129i309jmnaslklkmclkdñlkñsalLKMLklkmLKmLjkvtdfpassdegjhknjweyjkskwhfiNkjnknlkjnkVTFcmlkmlkmdAKLdAKDDADñ', //Permite encriptar la sesión.
//     resave: false, //La sesión no se guardará en cada petición, sino sólo se guardará si algo cambió 
//     saveUninitialized: false, //Asegura que no se guarde una sesión para una petición que no lo necesita
// }));
// /////////////////////////////////////////////////////////
// //Protección contra CSRF 

// const csrf= require('csurf');
// const csrfProtection= csrf();
// const csrfMiddleware = require('./util/csrf');
// app.use(csrfProtection);
// app.use(csrfMiddleware);

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Templates
app.set('view engine', 'ejs');
app.set('views', 'views'); 

//Módulos
//Login
const rutasLogin = require('./routes/login.js');
app.use('/login', rutasLogin);

//Empleado
const rutasEmpleado = require('./routes/empleado.js');
app.use('/empleado', rutasEmpleado);

//Paciente
const rutasPaciente = require('./routes/paciente.js');
app.use('/pacientes', rutasPaciente);

//historial
const rutasHistorial = require('./routes/historial.js');
app.use('/historial', rutasHistorial);


//Gestor de Datos
const rutasGestorDatos = require('./routes/gestor_datos.js');
app.use('/gestor_datos', rutasGestorDatos);

    

///////////////////////////////////////////////////////////////
// Poner un 404 a todas las rutas que son incorrectas:
app.use((request, response, next) => {
   
    response.status(404);
    response.send('Lo sentimos esta página no existe!'); //Manda la respuesta
});

app.listen(8000);
